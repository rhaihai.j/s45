import React from 'react';
import ReactDOM from 'react-dom';
import AppNavBar from './components/AppNavBar';
import Banner from './components/Banner'
import Highlights from './components/Highlights'

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'

import Home from './pages/Home';
import Courses from './pages/Courses';

ReactDOM.render(
  <React.StrictMode>
    <AppNavBar />
    <Courses />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
