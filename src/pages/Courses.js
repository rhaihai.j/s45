import Course from '../components/Course';

//bootstrap components
import Container from 'react-bootstrap/Container';

//data imports
import courses from '../mock-data/courses';

const Courses = () => {
    const CourseCards = courses.map((course) =>{
        return(
            <Course course={course} />
        )
        }) 
    // let CourseCards = [];
    // for(let i = 0; i<5; i++){
    //     CourseCards.push(<Course/>)
    // }
    //     console.log(CourseCards) 
    
   
    
    return(
        <Container fluid>
            {CourseCards}
        </Container>    
    );
}

export default Courses

