import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Container } from 'react-bootstrap'

const Course = (props) =>{
    let y = props.course;
    return(
        <Row>
            <Col>
            <Card className="card-highlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>{y.name}</h2>
                            <h4>{y.description}</h4>
                        </Card.Title>
                        <Card.Text>
                            Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco labo. Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco labo.
                        </Card.Text>
                    <h4>Price</h4>
                    <p>PhP 40,000</p>    
                    <Button variant="primary">Enroll</Button>    
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}

export default Course